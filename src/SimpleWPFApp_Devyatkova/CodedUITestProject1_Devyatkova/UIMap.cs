﻿namespace CodedUITestProject1_Devyatkova
{
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;


    public partial class UIMap
    {
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }
        /// <summary>
        /// SimpleAppTest_Devyatkova - Используйте "SimpleAppTest_DevyatkovaParams" для передачи параметров в этот метод.
        /// </summary>
        public void ModifiedSimpleAppTest_Devyatkova()
        {

            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            #endregion

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(17, 6));

            uICheckBoxCheckBox.WaitForControlEnabled();

            // Очистить "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTest_DevyatkovaParams.UICheckBoxCheckBoxChecked;
        }

        public virtual SimpleAppTest_DevyatkovaParams SimpleAppTest_DevyatkovaParams
        {
            get
            {
                if ((this.mSimpleAppTest_DevyatkovaParams == null))
                {
                    this.mSimpleAppTest_DevyatkovaParams = new SimpleAppTest_DevyatkovaParams();
                }
                return this.mSimpleAppTest_DevyatkovaParams;
            }
        }

        private SimpleAppTest_DevyatkovaParams mSimpleAppTest_DevyatkovaParams;
    }
    /// <summary>
    /// Параметры для передачи в "SimpleAppTest_Devyatkova"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class SimpleAppTest_DevyatkovaParams
    {

        #region Fields
        /// <summary>
        /// Очистить "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = false;
        #endregion
    }
}
